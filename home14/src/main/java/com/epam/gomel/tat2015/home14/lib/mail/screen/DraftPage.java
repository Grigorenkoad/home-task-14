package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class DraftPage extends MainPage{

    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");

    public void clickDeleteButton() {
        browser.click(DELETE_MESSAGE_BUTTON_LOCATOR);
    }

    public DraftPage markLetter(String subj) {
        String mailPattern = "//*[@title='" + subj + "']/../../../../../../label/input[@type='checkbox']";
        browser.click(By.xpath(mailPattern));
        return new DraftPage();
    }
}
