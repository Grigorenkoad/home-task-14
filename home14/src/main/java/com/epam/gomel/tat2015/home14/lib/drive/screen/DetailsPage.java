package com.epam.gomel.tat2015.home14.lib.drive.screen;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import org.openqa.selenium.By;

public class DetailsPage extends FilesPage{

    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//*[@data-click-action='resource.download']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'b-aside js-prevent-deselect ns-view-visible')]//*[@data-click-action='resource.delete']");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'b-aside js-prevent-deselect ns-view-visible')]//*[@data-click-action='resource.restore']");

    public void pressRemoveButton() {
        Browser.current().click(DELETE_BUTTON_LOCATOR);
    }

    public void pressRestoreButton() {
        Browser.current().click(RESTORE_BUTTON_LOCATOR);
    }

    public void pressDownloadButton() {
        Browser.current().click(DOWNLOAD_BUTTON_LOCATOR);
    }
}
