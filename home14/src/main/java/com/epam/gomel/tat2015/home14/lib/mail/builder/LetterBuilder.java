package com.epam.gomel.tat2015.home14.lib.mail.builder;

import com.epam.gomel.tat2015.home14.lib.common.CommonConstants;
import com.epam.gomel.tat2015.home14.lib.mail.BO.Letter;
import com.epam.gomel.tat2015.home14.framework.util.LetterUtils;

public class LetterBuilder {

    public static Letter getEmptyLetter() {
        Letter letter = new Letter();
        letter.setRecipient(CommonConstants.EMAIL);
        return letter;
    }

    public static Letter getFullLetter() {
        Letter letter = getEmptyLetter();
        letter.setSubject(LetterUtils.getSubject());
        letter.setBody(LetterUtils.getBody());
        return letter;
    }
}
