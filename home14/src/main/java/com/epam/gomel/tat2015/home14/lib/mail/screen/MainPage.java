package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class MainPage extends Page{
    public static final By HEADER_USER_NAME_LOCATOR = By.xpath("//*[@id='nb-1']/span[1]");

    public static final By INBOX_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#inbox']");
    public static final By SENT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#sent']");
    public static final By DRAFT_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#draft']");
    public static final By TRASH_LOCATOR = By.xpath("//div[@class='block-mail-left']//a[@href='#trash']");

    public String isTrueUserLogin() {
        return browser.read(HEADER_USER_NAME_LOCATOR);
    }

    public InboxPage openInboxPage() {
        browser.click(INBOX_LOCATOR);
        return new InboxPage();
    }

    public SentPage openSentPage() {
        browser.click(SENT_LOCATOR);
        return new SentPage();
    }

    public DraftPage openDraftPage() {
        browser.click(DRAFT_LOCATOR);
        return new DraftPage();
    }

    public TrashPage openTrashPage() {
        browser.click(TRASH_LOCATOR);
        return new TrashPage();
    }
}
