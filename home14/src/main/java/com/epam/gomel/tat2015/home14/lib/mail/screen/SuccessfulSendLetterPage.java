package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class SuccessfulSendLetterPage extends WriteLetterPage{
    public static final By SUCCESS_MESSAGE = By.className("b-done-title");

    public boolean isSuccessfulSendLetter() {
        return browser.isDisplayed(SUCCESS_MESSAGE);
    }
}
