package com.epam.gomel.tat2015.home14.tests.drive;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.drive.BO.CustomFile;
import com.epam.gomel.tat2015.home14.lib.drive.builder.FileBuilder;
import com.epam.gomel.tat2015.home14.lib.drive.service.DriveService;
import com.epam.gomel.tat2015.home14.lib.drive.service.LoginService;
import com.epam.gomel.tat2015.home14.framework.util.GenerateDataUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class RemoveSeveralFilesTest {

    Account account = AccountBuilder.getDefaultAccount();
    CustomFile file1 = FileBuilder.getCustomFile();
    CustomFile file2 = FileBuilder.getCustomFile();

    @BeforeClass
    public void loginAndUpload() {
        GenerateDataUtils.createFile(file1);
        GenerateDataUtils.createFile(file2);
        LoginService.login(account);
        DriveService.uploadFile(file1);
        DriveService.uploadFile(file2);
    }

    @Test
    public void removeFileTest() {
        List<CustomFile> files = new ArrayList<CustomFile>();
        files.add(file1);
        files.add(file2);
        DriveService.removeSeveralFiles(files);
        assertTrue(DriveService.isRemovedFileFromFiles(file1) && DriveService.isRemovedFileFromFiles(file2));
    }

    @AfterClass
    public void exit() {
        Browser.kill();
    }
}
