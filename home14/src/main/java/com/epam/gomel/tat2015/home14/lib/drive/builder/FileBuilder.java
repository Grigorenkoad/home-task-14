package com.epam.gomel.tat2015.home14.lib.drive.builder;

import com.epam.gomel.tat2015.home14.lib.drive.BO.CustomFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class FileBuilder {

    public static CustomFile getCustomFile() {
        CustomFile file = new CustomFile();
        file.setName(RandomStringUtils.randomAlphanumeric(5) + ".txt");
        file.setContext(RandomStringUtils.randomAlphanumeric(10));
        file.setPath(FileUtils.getTempDirectoryPath());
        return file;
    }
}
