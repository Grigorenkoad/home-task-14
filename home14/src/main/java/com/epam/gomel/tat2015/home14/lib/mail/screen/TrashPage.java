package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class TrashPage extends MainPage {

    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");
    private static final String mailPattern = "//*[@title='%s']/../../../../../../label/input[@type='checkbox']";

    public void clickDeleteButton() {
        browser.click(DELETE_BUTTON_LOCATOR);
    }

    public TrashPage markLetter(String subj) {
        String mail = String.format(mailPattern, subj);
        browser.click(By.xpath(mail));
        return new TrashPage();
    }

    public boolean isPresentLetter(String subj) {
        String mail = String.format(mailPattern, subj);
        return browser.isDisplayed(By.xpath(mail));
    }

}
