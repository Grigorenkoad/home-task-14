package com.epam.gomel.tat2015.home14.tests.drive;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.drive.BO.CustomFile;
import com.epam.gomel.tat2015.home14.lib.drive.builder.FileBuilder;
import com.epam.gomel.tat2015.home14.lib.drive.service.DriveService;
import com.epam.gomel.tat2015.home14.lib.drive.service.LoginService;
import com.epam.gomel.tat2015.home14.framework.util.GenerateDataUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class UploadFileTest {

    Account account = AccountBuilder.getDefaultAccount();
    CustomFile file = FileBuilder.getCustomFile();

    @BeforeClass
    public void login() {
        GenerateDataUtils.createFile(file);
        LoginService.login(account);
    }

    @Test
    public void uploadFileTest(){
        DriveService.uploadFile(file);
        assertTrue(DriveService.isFilePresentInFiles(file));
    }

    @AfterClass
    public void exit() {
        Browser.kill();
    }
}
