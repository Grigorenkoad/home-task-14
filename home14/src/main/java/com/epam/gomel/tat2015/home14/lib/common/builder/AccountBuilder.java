package com.epam.gomel.tat2015.home14.lib.common.builder;

import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.CommonConstants;
import org.apache.commons.lang3.RandomStringUtils;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setUserName(CommonConstants.USER_NAME);
        account.setPassword(CommonConstants.PASSWORD);
        account.setEmail(CommonConstants.EMAIL);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(3));
        return account;
    }
}
