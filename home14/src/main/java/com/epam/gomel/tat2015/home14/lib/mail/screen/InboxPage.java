package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class InboxPage extends MainPage {

    public static final By WRITE_MESSAGE_BUTTON_LOCATOR = By.xpath("//*[contains(@class, 'b-toolbar__item__label js-toolbar-item-title-compose')]");
    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");


    public WriteLetterPage openWriteMailPage() {
        browser.click(WRITE_MESSAGE_BUTTON_LOCATOR);
        return new WriteLetterPage();
    }

    public void deleteMail() {
        browser.click(DELETE_MESSAGE_BUTTON_LOCATOR);
    }
}
