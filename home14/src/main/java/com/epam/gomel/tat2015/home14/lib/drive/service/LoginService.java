package com.epam.gomel.tat2015.home14.lib.drive.service;

import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.drive.screen.LoginPage;
import com.epam.gomel.tat2015.home14.lib.drive.screen.TopMenuPage;
import ru.yandex.qatools.allure.annotations.Step;

public class LoginService {

    public static void login(Account account) {
        new LoginPage().open().fillUserNameInput(account.getUserName()).fillPasswordInput(account.getPassword()).pressLoginButton();
    }

    public static void login1(Account account) {
        new LoginPage().fillUserNameInput(account.getUserName()).fillPasswordInput(account.getPassword()).pressLoginButton();
    }

    public static boolean isSuccessfulLogin() {
        return new TopMenuPage().isSuccessfulLogin();
    }

    public static boolean isErrorMessageDisplayed() {
        return new LoginPage().isErrorDisplayed();
    }
}
