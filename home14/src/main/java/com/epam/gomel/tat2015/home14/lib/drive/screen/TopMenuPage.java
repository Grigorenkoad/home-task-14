package com.epam.gomel.tat2015.home14.lib.drive.screen;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import org.openqa.selenium.By;

public class TopMenuPage {

    public static final By FILES_LOCATOR = By.cssSelector("div[data-id='disk'] a[href$='/disk']");
    public static final By MENU_TRASH_LOCATOR = By.cssSelector("div[data-id='trash'] a[href$='/trash']");
    public static final By UPLOAD_INPUT_LOCATOR = By.className("button__attach");
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//*[@class='button__attach']/..");

    public static final By FINISHED_UPLOADING_LOCATOR = By.cssSelector(".b-item-upload__icon.b-item-upload__icon_done");
    public static final By CLOSE_UPLOAD_NOTIFICATION_LOCATOR = By.cssSelector("._nb-popup-close.ns-action.js-cross");

    public boolean isSuccessfulLogin() {
        return Browser.current().isDisplayed(UPLOAD_BUTTON_LOCATOR);
    }

    public FilesPage openFilesPage() {
        Browser.current().refresh();
        Browser.current().click(FILES_LOCATOR);
        return new FilesPage();
    }

    public TrashPage openTrashPage() {
        Browser.current().refresh();
        Browser.current().click(MENU_TRASH_LOCATOR);
        return new TrashPage();
    }

    public void uploadFile(String path) {
        Browser.current().waitForElementPresent(UPLOAD_BUTTON_LOCATOR);
        Browser.current().typeInUploadInput(UPLOAD_INPUT_LOCATOR, path);
        Browser.current().waitForElementPresent(FINISHED_UPLOADING_LOCATOR);
        Browser.current().click(CLOSE_UPLOAD_NOTIFICATION_LOCATOR);
    }

}
