package com.epam.gomel.tat2015.home14.tests.drive;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.drive.service.LoginService;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WrongLoginTest {

    Account account = AccountBuilder.getAccountWithWrongPass();

    @Test
    public void wrongLoginTest() {
        LoginService.login(account);
        assertTrue(LoginService.isErrorMessageDisplayed());
    }

    @AfterClass
    public void exit() {
        Browser.kill();
    }
}
