package com.epam.gomel.tat2015.home14.lib.mail.screen;

import org.openqa.selenium.By;

public class WriteLetterPage extends MainPage{
    public static final By RECIPIENT_LOCATOR = By.xpath("//tr[@class='b-compose-head__field b-compose-head__field_to js-compose-field-wrapper__to']//input[1]");
    public static final By SUBJECT_LOCATOR = By.name("subj");
    public static final By BODY_MAIL_LOCATOR = By.xpath("//textarea[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public static final By ERROR_MESSAGE_LOCATOR = By.cssSelector("tr.b-compose-head__field.b-compose-head__field_to.js-compose-field-wrapper__to span.b-notification.b-notification_error.b-notification_error_required span.b-notification__i");

    public static final By SAVED_LOCATOR = By.cssSelector("div[data-compose-type='letter postcard'] .b-compose-message__actions__helper_saved");

    public WriteLetterPage fillRecipientInput(String recipient) {
        browser.type(RECIPIENT_LOCATOR, recipient);
        return new WriteLetterPage();
    }

    public WriteLetterPage fillSubjectInput(String subject) {
        browser.type(SUBJECT_LOCATOR, subject);
        return new WriteLetterPage();
    }

    public WriteLetterPage fillBodyInput(String body) {
        browser.type(BODY_MAIL_LOCATOR, body);
        return new WriteLetterPage();
    }

    public SuccessfulSendLetterPage pressSubmitButton() {
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        return new SuccessfulSendLetterPage();
    }

    public boolean isErrorMessageDisplayed() {
        return browser.isDisplayed(ERROR_MESSAGE_LOCATOR);
    }

    public boolean isSavedAsDraftLetter() {
        return browser.isDisplayed(SAVED_LOCATOR);
    }
}
