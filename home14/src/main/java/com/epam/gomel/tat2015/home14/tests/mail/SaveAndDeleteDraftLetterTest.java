package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.mail.BO.Letter;
import com.epam.gomel.tat2015.home14.lib.mail.builder.LetterBuilder;
import com.epam.gomel.tat2015.home14.lib.mail.service.LoginService;
import com.epam.gomel.tat2015.home14.lib.mail.service.MailService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;


public class SaveAndDeleteDraftLetterTest {

    Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.getFullLetter();


    @BeforeClass
    public void login() {
        LoginService.login(account);
    }

    @Test
    public void saveLetterAsDraftTest() {
        assertTrue(MailService.isSavedDraftLetter(letter));
    }

    @Test(dependsOnMethods = "saveLetterAsDraftTest")
    public void deleteLetterFromDraftTest() {
        MailService.deleteDraftLetter(letter);
        assertTrue(MailService.isLetterPresentInTrash(letter));
    }

    @Test(dependsOnMethods = "deleteLetterFromDraftTest")
    public void deleteLetterFromTrashTest() {
        assertTrue(MailService.deleteLetterFromTrash(letter));
    }

}
