package com.epam.gomel.tat2015.home14.lib.mail.screen;

import com.epam.gomel.tat2015.home14.lib.common.CommonConstants;
import org.openqa.selenium.By;

public class LoginPage extends Page{
    public static final By INPUT_LOGIN_LOCATOR = By.xpath("//input[@name='login']");
    public static final By INPUT_PASSWORD_LOCATOR = By.xpath("//input[@name='passwd']");
    public static final By BUTTON_SUBMIT_LOCATOR = By.xpath("//span/button[@type='submit']");

    public static final By ERROR_LOGIN_MESSAGE_LOCATOR = By.className("error-msg");


    public LoginPage open(){
        browser.open(CommonConstants.YANDEX_MAIL_START_PAGE);
        return this;
    }

    public LoginPage fillUserNameInput(String name)
    {
        browser.type(INPUT_LOGIN_LOCATOR, name);
        return new LoginPage();
    }

    public LoginPage fillPasswordInput(String pass)
    {
        browser.type(INPUT_PASSWORD_LOCATOR, pass);
        return new LoginPage();
    }

    public MainPage pressLoginButton(){
        browser.click(BUTTON_SUBMIT_LOCATOR);
        return new MainPage();
    }

    public boolean isErrorDisplayed(){
        return browser.isDisplayed(ERROR_LOGIN_MESSAGE_LOCATOR);
    }

}
