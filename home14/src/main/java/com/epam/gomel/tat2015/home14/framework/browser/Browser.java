package com.epam.gomel.tat2015.home14.framework.browser;

import com.epam.gomel.tat2015.home14.framework.config.GlobalConfig;
import com.epam.gomel.tat2015.home14.framework.util.GenerateDataUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import com.epam.gomel.tat2015.home14.framework.report.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Browser {
    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = 20;
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = 15;
    private static final int WAIT_ELEMENT_TIMEOUT = 20;

    private WebDriver driver;

    private static Browser instance = null;

    private Browser(WebDriver driver) {
        this.driver = driver;
    }

    public static Browser current() {
        if (instance != null) {
            return instance;
        }
        return instance = init();
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    private static Browser init() {
        WebDriver driver;
        switch (GlobalConfig.getInstance().getBrowserType()) {
            default:
            case FIREFOX:
                Logger.info("Initialising firefox browser.");
                driver = getFireFoxDriver();
                break;
            case CHROME:
                Logger.info("Initialising chrome browser.");
                driver = getChromeDriver();
                break;
        }

        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return new Browser(driver);
    }

    private static WebDriver getFireFoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", GenerateDataUtils.getDownloadDirectoryPath());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        if (GlobalConfig.getInstance().getSeleniumHub().equals("") || GlobalConfig.getInstance().getSeleniumHub() == null)
            return new FirefoxDriver(profile);
        else {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability(FirefoxDriver.PROFILE, profile);
            try {
                return new RemoteWebDriver(new URL(GlobalConfig.getInstance().getSeleniumHub()), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return new FirefoxDriver(profile);
            }
        }
    }

    private static WebDriver getChromeDriver() {
        Map<String, Object> map = new Hashtable<>();
        map.put("profile.default_content_settings.popups", 0);
        map.put("download.prompt_for_download", "false");
        map.put("download.default_directory", GenerateDataUtils.getDownloadDirectoryPath());
        map.put("download.directory_upgrade", true);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("chromeOptions", map);
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        if (GlobalConfig.getInstance().getSeleniumHub().equals("") || GlobalConfig.getInstance().getSeleniumHub() == null)
            return new ChromeDriver(options);
        else {
            try {
                return new RemoteWebDriver(new URL(GlobalConfig.getInstance().getSeleniumHub()), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return new ChromeDriver(options);
            }
        }
    }

    public void open(String url) {
        Logger.info("Going to URL: " + url);
        driver.get(url);
    }

    public static void kill() {
        if (instance != null) {
            try {
                instance.driver.quit();
            } finally {
                instance = null;
            }
        }
    }

    public void waitForElementPresent(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

    public void waitForElementEnabled(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitForElementVisible(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void highlightElement(By locator) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='5px solid green'", driver.findElement(locator));
    }

    public void unHighlightElement(By locator) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='0px'", driver.findElement(locator));
    }

    public void click(final By locator) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
        highlightElement(locator);
        Logger.info("Clicking element '" + driver.findElement(locator).getText() + "' (Located: " + locator + ")");
        takeScreenshot();
        unHighlightElement(locator);
        driver.findElement(locator).click();
    }

    public void type(final By locator, String text) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
        highlightElement(locator);
        Logger.info("Typing text '" + text + "' to input form '" + driver.findElement(locator).getAttribute("name") + "' (Located: " + locator + ")");
        driver.findElement(locator).sendKeys(text);
        takeScreenshot();
        unHighlightElement(locator);
    }

    public void typeInUploadInput(final By locator, String text) {
        highlightElement(locator);
        Logger.info("Typing text '" + text + "' to input form '" + driver.findElement(locator).getAttribute("name") + "' (Located: " + locator + ")");
        driver.findElement(locator).sendKeys(text);
        takeScreenshot();
        driver.findElement(locator).sendKeys(text);
    }

    public String read(final By locator) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
        highlightElement(locator);
        Logger.info("Reading text: " + driver.findElement(locator).getText());
        takeScreenshot();
        unHighlightElement(locator);
        return driver.findElement(locator).getText();
    }

    public void dragAndDrop(final By locator, final By targetLocator) {
        waitForElementPresent(locator);
        waitForElementVisible(locator);
        waitForElementEnabled(locator);
        waitForElementPresent(targetLocator);
        waitForElementVisible(targetLocator);
        waitForElementEnabled(targetLocator);
        WebElement element = driver.findElement(locator);
        WebElement target = driver.findElement(targetLocator);
        highlightElement(locator);
        highlightElement(targetLocator);
        takeScreenshot();
        Logger.info("Dragging element '" + driver.findElement(locator).getText() + "' (Located: " + locator + ")" +
                "to '" + driver.findElement(targetLocator).getText() + "' (Located: " + targetLocator + ")");
        (new Actions(driver)).dragAndDrop(element, target).perform();
        unHighlightElement(targetLocator);
    }

    public By selectSeveralElements(List<By> locators) {
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL);
        WebElement element;
        for (By locator : locators) {
            waitForElementPresent(locator);
            waitForElementVisible(locator);
            waitForElementEnabled(locator);
            highlightElement(locator);
            Logger.info("Clicking element '" + driver.findElement(locator).getText() + "' (Located: " + locator + ")");
            element = driver.findElement(locator);
            action.moveToElement(element).click();
        }

        takeScreenshot();
        action.keyUp(Keys.CONTROL).perform();
        return locators.get(0);
    }

    public boolean isDisplayed(By locator) {
        waitForElementPresent(locator);
        boolean succeed = driver.findElements(locator).size() > 0;
        if (succeed) {
            Logger.info("Element " + driver.findElement(locator).getText() + " is present.");
            highlightElement(locator);
            takeScreenshot();
            unHighlightElement(locator);
        } else Logger.error("Element " + driver.findElement(locator).getText() + " is not present.");
        return succeed;
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public void takeScreenshot() {
        try {
            byte[] screenshotBytes = ((TakesScreenshot) Browser.current().getWrappedDriver()).getScreenshotAs(OutputType.BYTES);
            File screenshotFile = new File(screenshotName());
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Logger.save(screenshotFile.getName());
        } catch (Exception e) {
            Logger.error("Failed to write screenshot: " + e.getMessage(), e);
        }
    }

    private static String screenshotName() {
        return GlobalConfig.getInstance().getResultDir() + File.separator + System.nanoTime() + ".png";
    }
}
