package com.epam.gomel.tat2015.home14.framework.util;

import org.apache.commons.lang3.RandomStringUtils;

public class LetterUtils {

    public static String getSubject() {
        return RandomStringUtils.randomAlphanumeric(7);
    }

    public static String getBody() {
        return RandomStringUtils.randomAlphanumeric(30);
    }

}
