package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.mail.BO.Letter;
import com.epam.gomel.tat2015.home14.lib.mail.builder.LetterBuilder;
import com.epam.gomel.tat2015.home14.lib.mail.service.LoginService;
import com.epam.gomel.tat2015.home14.lib.mail.service.MailService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SendFullLetterTest {

    Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.getFullLetter();


    @BeforeClass
    public void login() {
        LoginService.login(account);
    }

    @Test
    public void sendFullLetterTest() {
        MailService.sendFullLetter(letter);
        assertTrue(MailService.isSuccessfulSendLetter() && MailService.isLetterPresentInSent(letter));
    }

}
