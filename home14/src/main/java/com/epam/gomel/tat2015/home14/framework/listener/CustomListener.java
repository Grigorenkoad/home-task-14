package com.epam.gomel.tat2015.home14.framework.listener;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import com.epam.gomel.tat2015.home14.lib.common.Test;
import com.epam.gomel.tat2015.home14.framework.report.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class CustomListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext context) {
        Test.setName(context.getName());
        Test.setSuitName(context.getSuite().getName());
        Test.setStartDate(context.getStartDate().toString());
        super.onStart(context);
        Logger.info("Started test " + context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        super.onFinish(context);
        Logger.info("Finished test " + context.getName());
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        super.onTestFailure(testResult);
        Browser.current().takeScreenshot();
        Logger.error("Failed test  " + testResult.getThrowable().getMessage(), testResult.getThrowable());
    }
}
