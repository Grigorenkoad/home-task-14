package com.epam.gomel.tat2015.home14.lib.mail.screen;

import com.epam.gomel.tat2015.home14.lib.mail.BO.Letter;
import org.openqa.selenium.By;

public class SentPage extends MainPage {

    public boolean isLetterPresent(Letter letter) {
        String letterPattern = "//*[@title='" + letter.getSubject() + "']";
        return browser.isDisplayed(By.xpath(letterPattern));
    }
}
