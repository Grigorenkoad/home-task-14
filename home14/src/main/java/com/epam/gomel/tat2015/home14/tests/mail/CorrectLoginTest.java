package com.epam.gomel.tat2015.home14.tests.mail;

import com.epam.gomel.tat2015.home14.lib.common.BO.Account;
import com.epam.gomel.tat2015.home14.lib.common.builder.AccountBuilder;
import com.epam.gomel.tat2015.home14.lib.mail.service.LoginService;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CorrectLoginTest {

    Account account = AccountBuilder.getDefaultAccount();

    @Test
    public void correctLoginTest() {
        LoginService.login(account);
        assertTrue(LoginService.isSuccessfulLogin(account));
    }

}
