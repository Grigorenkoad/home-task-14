package com.epam.gomel.tat2015.home14.lib.mail.service;

import com.epam.gomel.tat2015.home14.lib.mail.BO.Letter;
import com.epam.gomel.tat2015.home14.lib.mail.screen.*;

public class MailService {

    public static void sendFullLetter(Letter letter) {
        new MainPage()
                .openInboxPage()
                .openWriteMailPage()
                .fillRecipientInput(letter.getRecipient())
                .fillSubjectInput(letter.getSubject())
                .fillBodyInput(letter.getBody())
                .pressSubmitButton();
    }

    public static void sendLetterWithoutRecipient(Letter letter) {
        new MainPage()
                .openInboxPage()
                .openWriteMailPage()
                .fillSubjectInput(letter.getSubject())
                .fillBodyInput(letter.getBody())
                .pressSubmitButton();

    }

    public static boolean isSavedDraftLetter(Letter letter) {
        return new MainPage()
                .openInboxPage()
                .openWriteMailPage()
                .fillSubjectInput(letter.getSubject())
                .fillBodyInput(letter.getBody())
                .isSavedAsDraftLetter();
    }

    public static boolean deleteLetterFromTrash(Letter letter) {
        new MainPage()
                .openTrashPage()
                .markLetter(letter.getSubject())
                .clickDeleteButton();
        return true;
    }

    public static void deleteDraftLetter(Letter letter) {
        new MainPage().openDraftPage().markLetter(letter.getSubject()).clickDeleteButton();
    }

    public static boolean isLetterPresentInTrash(Letter letter) {
        return new MainPage().openTrashPage().isPresentLetter(letter.getSubject());
    }

    public static boolean isSuccessfulSendLetter() {
        return new SuccessfulSendLetterPage().isSuccessfulSendLetter();
    }

    public static boolean isLetterPresentInSent(Letter letter) {
        return new MainPage().openSentPage().isLetterPresent(letter);
    }

    public static boolean isErrorMessageDisplayed() {
        return new WriteLetterPage().isErrorMessageDisplayed();
    }
}
