package com.epam.gomel.tat2015.home14.lib.drive.screen;

import com.epam.gomel.tat2015.home14.framework.browser.Browser;
import org.openqa.selenium.By;

public class TrashPage extends TopMenuPage {

    public static final String FILE_LOCATOR = "//div[contains(@data-id, 'trash/%s')]";

    public DetailsPage markFile(String name) {
        Browser.current().click(By.xpath(String.format(FILE_LOCATOR, name)));
        return new DetailsPage();
    }

    public boolean isFilePresent(String name) {
        return Browser.current().isDisplayed(By.xpath(String.format(FILE_LOCATOR, name)));
    }

}
